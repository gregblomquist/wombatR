# README #

`wombatR` is a simple `R` wrapper for calling Karin Meyer's [WOMBAT](http://didgeridoo.une.edu.au/km/wombat.php) program. 
This is a fork of Matthieu Bruneaux's [original package](https://github.com/mdjbru-R-packages/wombatR). 

**NB**  The package has only been tested on linux. On Ubuntu I had to install some additional dependencies with `sudo apt install libgmp3-dev` for `wombatR` to install successfully. WOMBAT is assumed to be installed as `~/bin/wombat`.

```r
install.packages('devtools')
devtools::install_gitlab("gregblomquist/wombatR")
```

Once installed, just load it like any other package.

```r
library(wombatR)
```
